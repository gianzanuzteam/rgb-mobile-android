package com.example.gianz.rgbmobile;

import android.bluetooth.BluetoothSocket;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.view.View;
import android.widget.Toast;

import com.skydoves.colorpickerview.ColorEnvelope;
import com.skydoves.colorpickerview.ColorPickerView;
import com.skydoves.colorpickerview.listeners.ColorEnvelopeListener;
import com.skydoves.colorpickerview.sliders.BrightnessSlideBar;

import java.io.IOException;
import java.io.OutputStream;

import androidx.appcompat.app.AppCompatActivity;

public class ConnectedActivity extends AppCompatActivity {

    private OutputStream outputStream;
    private BluetoothSocket bluetoothSocket;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connected);

        /* Orientação padrão PORTRATT */
        setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);

        bluetoothSocket = WaitConnectActivity.getBluetoothSocket();

        try{
            outputStream = bluetoothSocket.getOutputStream();
            int[] RGB = {255,255,255};
            setColour(RGB);
        }
        catch (IOException e)
        {
            Toast.makeText(getApplicationContext(), "Erro de fluxo inicio", Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        /* Define evento para trocar de cor */
        ColorPickerView colorPickerView = findViewById(R.id.colorPickerView);
        colorPickerView.setColorListener(new ColorEnvelopeListener() {
            @Override
            public void onColorSelected(ColorEnvelope colorEnvelope, boolean fromUser) {
                int[] RGBA = colorEnvelope.getArgb();
                int[] RGB = {0, 0, 0};
                RGB[0] = RGBA[1];
                RGB[1] = RGBA[2];
                RGB[2] = RGBA[3];
                setColour(RGB);
            }
        });

        BrightnessSlideBar brightnessSlideBar = findViewById(R.id.brightnessSlide);
        colorPickerView.attachBrightnessSlider(brightnessSlideBar);
    }

    @Override
    public void onBackPressed() {
        try {
            outputStream.close();
            bluetoothSocket.close();
        }
        catch (IOException e)
        {
            return;
        }

        super.onBackPressed();
        finish();
    }

    public void onImageButton(View view)
    {
        ColorPickerView colorPickerView = findViewById(R.id.colorPickerView);

        switch (view.getId())
        {
            case R.id.imageButtonBlack:
                colorPickerView.selectByHsv(0x000000);
                break;
            case R.id.imageButtonRed:
                colorPickerView.selectByHsv(0xff0000);
                break;
            case R.id.imageButtonGreen:
                colorPickerView.selectByHsv(0x00ff00);
                break;
            case R.id.imageButtonBlue:
                colorPickerView.selectByHsv(0x0000ff);
                break;
            case R.id.imageButtonYellow:
                colorPickerView.selectByHsv(0xffff00);
                break;
            case R.id.imageButtonWhite:
                colorPickerView.selectByHsv(0xffffff);
                break;
        }
    }

    private void setColour(int[] RGB)
    {
        try {
            outputStream.write(("#SET_COLOUR\n").getBytes());
            outputStream.write(((255 - RGB[0]) + "\n").getBytes());
            outputStream.write(((255 - RGB[1]) + "\n").getBytes());
            outputStream.write(((255 - RGB[2]) + "\n").getBytes());
            outputStream.write(("#END\n").getBytes());
        }
        catch (IOException e)
        {
            Toast.makeText(getApplicationContext(), "Erro de fluxo", Toast.LENGTH_LONG).show();
            finish();
            return;
        }
    }
}
