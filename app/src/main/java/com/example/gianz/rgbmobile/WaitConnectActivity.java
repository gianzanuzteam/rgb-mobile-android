package com.example.gianz.rgbmobile;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import java.io.IOException;
import java.util.UUID;

import androidx.appcompat.app.AppCompatActivity;

public class WaitConnectActivity extends AppCompatActivity {
    BluetoothConnect bluetoothConnect;
    private static BluetoothSocket bluetoothSocket;
    private String deviceName;
    private String deviceAddress;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_wait_connect);

        /* Obtém o nome do dispositivo */
        deviceName = getIntent().getStringExtra("deviceName");
        TextView myText = (TextView) findViewById(R.id.textViewConnecting);
        myText.setText("Conectando ao dispositivo " + deviceName);

        /* Inicializa thread bluetooth */
        deviceAddress = getIntent().getStringExtra("deviceAddress");
        bluetoothConnect = new BluetoothConnect();
        bluetoothConnect.execute(deviceAddress);
    }

    @Override
    public void onBackPressed() {
        bluetoothConnect.cancel(true);
        super.onBackPressed();
        finish();
    }

    public static BluetoothSocket getBluetoothSocket() {
        return bluetoothSocket;
    }

    public class BluetoothConnect extends AsyncTask<String, Void, String>
    {
        @Override
        protected String doInBackground(String... address)
        {
            /* Inicializa adaptador Bluetooth */
            BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
            if (mBluetoothAdapter == null) {
                return "Erro no adaptador Bluetooth";
            }
            if (!mBluetoothAdapter.isEnabled()) {
                return "Bluetooth desligado";
            }

            /* Obtém o dispositivo a partir do MAC */
            if(!BluetoothAdapter.checkBluetoothAddress(address[0]))
            {
                return "Dispositivo inválido";
            }
            BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address[0]);

            /* Obtém socket a partir do device */
            try {
                UUID uuid = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
                bluetoothSocket = device.createRfcommSocketToServiceRecord(uuid);
            }
            catch(IOException e) {
                return "Não foi possível obter o socket de conexão";
            }

            /* Verifica se socket já está aberto */
            if(bluetoothSocket.isConnected())
            {
                /* Tenta finalizar o socket */
                try {
                    bluetoothSocket.close();
                }
                catch(IOException e) {
                    return "Não foi possível finalizar conexão com o socket";
                }
            }

            /* Inicializa socket */
            try{
                bluetoothSocket.connect();
            }
            catch(IOException e) {
                return "Não foi possível obter conexão com o dispositivo";
            }

            return "Inicializado com sucesso";
        }

        @Override
        protected void onPostExecute(String string){
            /* Mostra texto na tela */
            Toast.makeText(getApplicationContext(), string, Toast.LENGTH_LONG).show();

            /* Verifica se obteve o socket */
            if(bluetoothSocket == null)
            {
                finish();
                return;
            }

            /* Verifica se obteve conexão */
            if(bluetoothSocket.isConnected())
            {
                Intent intent = new Intent();
                intent.setClass(getApplicationContext(), ConnectedActivity.class);
                startActivity(intent);
                finish();
            }
            else
            {
                finish();
                return;
            }
        }
    }


}
