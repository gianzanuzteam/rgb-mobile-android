package com.example.gianz.rgbmobile;

import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    private Set<BluetoothDevice> pairedDevices;

    /* Array para guardar informações dos dispositivos */
    List<String> nameArray = new ArrayList<>();
    List<String> infoArray = new ArrayList<>();
    List<Integer> imageArray = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        /* Inicializa adaptador Bluetooth */
        BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();
        if (mBluetoothAdapter == null) {
            return;
        }
        if (!mBluetoothAdapter.isEnabled()) {
            return;
        }

        /* Verifica dispositivos pareados */
        Set<BluetoothDevice> pairedDevices = mBluetoothAdapter.getBondedDevices();
        for (BluetoothDevice device : pairedDevices) {
            if(device.getName().contains("RGB"))
            {
                /* Salva na lista */
                nameArray.add(device.getName());
                infoArray.add(device.getAddress());
                imageArray.add(R.drawable.icons);
            }
        }

        /* Atualiza a ListView */
        CustomListAdapter customListAdapter = new CustomListAdapter(this, nameArray.toArray(new String[0]), infoArray.toArray(new String[0]), imageArray.toArray(new Integer[0]));
        ListView listView = (ListView) findViewById(R.id.listviewID);
        listView.setAdapter(customListAdapter);

        listView.setOnItemClickListener(new OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Intent intent = new Intent(MainActivity.this, WaitConnectActivity.class);
                intent.putExtra("deviceName", nameArray.get(position));
                intent.putExtra("deviceAddress", infoArray.get(position));
                startActivity(intent);
            }
        });
    }
}
